import React, { useState } from "react";
import "./estilos.css";

import {Bola} from "./Ejercicios.jsx";

export default () => {
    
    const colorBola = "grey";

    const lightOut = (isOn) => {
        let ret;
        
        (isOn)? ret = false : ret = true;
        console.log(ret);
        return ret;
    }

    return (
    <>
        <Bola color="red" colorBola={colorBola} lightOut={lightOut} lightOn={false}/>
        <Bola color="yellow" colorBola={colorBola} lightOut={lightOut} lightOn={false}/>
        <Bola color="green" colorBola={colorBola} lightOut={lightOut} lightOn={false}/>
    </>
)};