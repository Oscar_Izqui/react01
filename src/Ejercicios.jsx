import React, { useState } from "react";
import styled from "styled-components"
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import "./estilos.css";

const FirstCap = styled.div`
    text-align: center;
    align-self: center;
`;

const Capital = (props) => (
    <FirstCap>
        <div className="inicial">{props.nombre[0]}</div>
        <div className="ciudad">{props.nombre}</div>
    </ FirstCap>
);

const Bola = (props) => {

    let colorPintao = props.colorBola;

    const clicar = () => {
        if (props.lightOn) {
            colorPintao = props.color;
            props.lightOut();
        }
        else {
            colorPintao = props.colorBola;
            props.lightOut();
        }
    }

    return (
        <div onClick={clicar} style={{ backgroundColor: colorPintao, width: "40px", height: "40px", borderRadius: "40px" }}> </div>
    );
};

const Icono = styled.i`
    font-size: 3em;
    display: block;
`;

const Flipicon = (props) => {

    const [icono, setIcono] = useState(props.icon1);

    const iconClick = () => {
        (icono === props.icon2) ? setIcono(props.icon1) : setIcono(props.icon2);
    }

    return (
        <Icono onClick={iconClick} className={"fa " + icono} aria-hidden="true"></Icono>
    );
};


const PagesBar = (props) => {

    const [minPage, setMinPage] = useState(1);

    const PageNext = () => {
        if (!(minPage + 4 == props.maxPages)) setMinPage(minPage + 1);
    }
    const PagePrev = () => {
        if (!(minPage - 1 == 0)) setMinPage(minPage - 1);
    }
    const PageFirst = () => {
        setMinPage(1);
    }
    const PageLast = () => {
        setMinPage(props.maxPages - 4);
    }

    return (
      <Pagination aria-label="Page navigation example">
      <PaginationItem>
          <PaginationLink onClick={PageFirst} first />
        </PaginationItem>
        <PaginationItem>
          <PaginationLink onClick={PagePrev} previous />
        </PaginationItem>
        <PaginationItem>
          <PaginationLink>
            {minPage}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink>
          {minPage + 1}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink>
          {minPage + 2}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink>
          {minPage + 3}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink>
            {minPage + 4}
          </PaginationLink>
        </PaginationItem>
        <PaginationItem>
          <PaginationLink onClick={PageNext} next />
        </PaginationItem>
        <PaginationItem>
          <PaginationLink last onClick={PageLast    } />
        </PaginationItem>
      </Pagination>
    );
  }

export { Capital, Bola, Flipicon, PagesBar };