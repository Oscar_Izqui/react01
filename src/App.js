import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./estilos.css";

import {Capital, Flipicon, PagesBar} from "./Ejercicios.jsx";
import Instalacion from "./Electricidad.js"
import Semaforo from "./Semaforo";

export default () => (
  <>   
    <div className="lacity"> <Capital nombre="Barcelona" /> </div>
    {/*<Semaforo />*/}
    <Flipicon icon1="fa-thumbs-o-up" icon2="fa-thumbs-o-down" />
    <Flipicon icon1="fa-chevron-circle-up" icon2="fa-chevron-circle-down" />
    <PagesBar maxPages="20"/>
    <Instalacion />
  </>
);