import React, { useState } from "react";
import ReactCSSTransitionGroup from 'react-transition-group';
import styled, { css, keyframes } from "styled-components"

import Loff from "url:../img/off.jpg"
import Lon from "url:../img/on.jpg"
import Boff from "url:../img/boff.jpg"
import Bon from "url:../img/bon.jpg"

const CircuitoElec = styled.div`
    background-color: black;
    width: 535px;
    height: 700px;
    position: relative;
`;

const Interruptor = styled.img`
    position: absolute;
    top: 0px;
    z-index: 10;
`;

const bombilla = (initialOpacity, finalOpacity) => {
return ( keyframes`
    from {
        opacity: ${initialOpacity};
    }

    to {
        opacity: ${finalOpacity};
    }
`)};

const encendiendo = props => css`${bombilla(1, 0)} ${props.duracion} linear;`;

const isOpac = props => css`${props.opacidad}`;

const Bombilla = styled.img`
    animation: ${encendiendo};
    opacity: ${isOpac};
    position: absolute;
    top: 0px;
    left: 0px;
    transition: opacity 3s;

`;

const BombillaApagada = styled.img`
    position: absolute;
    top: 0px;
    left: 0px;
`;

const Instalacion = () => {

    const [buttonStatus, setButtonStatus] = useState(Boff);
    const [lighted, setLighted] = useState(0);

    const SwitchClick = () => {
        if (buttonStatus == Bon) {
            setButtonStatus(Boff);
            setLighted(0);
        }
        else {
            setButtonStatus(Bon);
            setLighted(1);
        };
    }

    return (
        <CircuitoElec >
            <Interruptor src={buttonStatus} onClick={SwitchClick}></Interruptor>            
            <BombillaApagada src={Loff}></BombillaApagada>
            <Bombilla opacidad={lighted} duracion="2s" src={Lon}></Bombilla>
        </CircuitoElec>
    );
};

const Image = () => {
    const [wobble, setWobble] = React.useState(0)
    return (
        <img
            className={styles.image}
            src="https://source.unsplash.com/random/400x200"
            alt="randomised!"
            onClick={() => setWobble(1)}
            onAnimationEnd={() => setWobble(0)}
            wobble={wobble}
        />
    )
}

export default Instalacion;